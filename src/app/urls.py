from django.urls import include, path
from market.views import MarketHomeView
from django.contrib import admin


urlpatterns = [

    path('', MarketHomeView.as_view()),
]