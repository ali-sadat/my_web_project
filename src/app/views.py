# some_app/views.py
from django.views.generic import TemplateView

class MarketHomeView(TemplateView):
    template_name = "home.html"