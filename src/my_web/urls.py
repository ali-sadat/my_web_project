from django.urls import include, path
from django.contrib import admin


urlpatterns = [
    # Examples:
    # url(r'^$', 'tele_web.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    #path('', views.index, name='index'),
    path('market/', include('market.urls')),

    path('admin/', admin.site.urls),
]

